import json
import os
import time

import psutil
from kafka import KafkaProducer

KAFKA_HOST = os.environ.get('KAFKA_HOST', '192.168.1.3')

if __name__ == '__main__':
    producer = KafkaProducer(bootstrap_servers=[f'{KAFKA_HOST}:9092'])

    while True:
        cpu = f'CPU Load: {psutil.cpu_percent()}'
        mem = f'Mem: {psutil.virtual_memory().percent}'
        data = {
            'cpu': cpu,
            'memory': mem
        }
        json_data = json.dumps(data)
        print(data)
        producer.send('rpi_status', bytes(json_data, 'utf-8'))
        time.sleep(2)
