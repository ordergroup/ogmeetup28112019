import json
import os
import threading

from kafka import KafkaConsumer
from display_text import DisplayText

KAFKA_HOST = os.environ.get('KAFKA_HOST', '192.168.1.3')

if __name__ == '__main__':
    consumer = KafkaConsumer(
        'rpi_consumer',
        bootstrap_servers=f'{KAFKA_HOST}:9092',
    )
    display_text = DisplayText()
    t = threading.Thread(target=display_text.run)
    t.start()
    print('Starting message consumer')
    for message in consumer:
        value = json.loads(message.value)
        message = value.get('message', None)
        print('Getting text {text}'.format(text=message))
        display_text.set_text(message)