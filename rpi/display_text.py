# Copyright (c) 2017 Adafruit Industries
# Author: Tony DiCola & James DeVito
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in
# all copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
# THE SOFTWARE.
import time

import Adafruit_SSD1306

from PIL import Image
from PIL import ImageDraw
from PIL import ImageFont

class DisplayText:

    def __init__(self):
        # Raspberry Pi pin configuration:
        RST = None
        DC = 23
        SPI_PORT = 0
        SPI_DEVICE = 0

        self.disp = Adafruit_SSD1306.SSD1306_128_32(rst=RST)

        self.disp.begin()

        # Clear display.
        self.disp.clear()
        self.disp.display()

        self.width = self.disp.width
        self.height = self.disp.height
        self.image = Image.new('1', (self.width, self.height))

        self.draw = ImageDraw.Draw(self.image)

        self.draw.rectangle((0,0,self.width,self.height), outline=0, fill=0)

        padding = -2
        self.top = padding

        self.font = ImageFont.truetype('upheavtt.ttf',32)

        self.x = 0
        self.txt = "    "
        self.i = 0

    def run(self):
        while True:
            self.draw.rectangle((0,0,self.width,self.height), outline=0, fill=0)
            self.draw.text((self.x, self.top), self.txt[self.i:self.i+8],  font=self.font, fill=255)
            self.i = self.i + 1
            # Display image.
            self.disp.image(self.image)
            self.disp.display()
            time.sleep(0.4)
            if self.i > len(self.txt):
                self.i = 0

    def set_text(self, text):
        self.txt = text
        self.i = 0
