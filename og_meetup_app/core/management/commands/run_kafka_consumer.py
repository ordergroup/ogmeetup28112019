import json

from asgiref.sync import async_to_sync
from django.conf import settings
from django.core.management.base import BaseCommand, CommandError
from channels.layers import get_channel_layer
from kafka import KafkaConsumer

channel_layer = get_channel_layer()

KAFKA_HOST = settings.KAFKA_HOST


class Command(BaseCommand):

    async def send_to_websocket(self, message: str):
        await channel_layer.group_send(
            'notifications',
            {
                'message': message,
                'type': 'notification',
            }
        )

    def send_status_to_ws(self, message):
        async_to_sync(self.send_to_websocket)(message=message)

    def handle(self, *args, **options):
        consumer = KafkaConsumer(
            'rpi_status',
            bootstrap_servers=f'{KAFKA_HOST}:9092',
        )

        for message in consumer:
            value = json.loads(message.value)
            print(f'Data: {value}')
            self.send_status_to_ws(value)