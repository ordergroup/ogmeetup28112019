import json
from datetime import datetime

from channels.generic.websocket import AsyncWebsocketConsumer, AsyncJsonWebsocketConsumer
from django.conf import settings
from kafka import KafkaProducer

KAFKA_HOST = settings.KAFKA_HOST


class NotificationsConsumer(AsyncWebsocketConsumer):

    async def connect(self):

        await self.channel_layer.group_add(
            'notifications',
            self.channel_name
        )

        await self.accept()

    async def receive(self, *, text_data):
        data = bytes(text_data, 'utf-8')
        producer = KafkaProducer(bootstrap_servers=[f'{KAFKA_HOST}:9092'])
        producer.send('rpi_consumer', data)
        print(f'Sending data: {data}')
        producer.flush()
        producer.close(10)

    async def disconnect(self, message):
        pass

    async def notification(self, event):
        message = event['message']

        # Send message to WebSocket
        await self.send(text_data=json.dumps({
            'message': message,
            'time': datetime.now().strftime("%H:%M:%S")
        }))